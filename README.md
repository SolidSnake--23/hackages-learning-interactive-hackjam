# Interactive Hackjam

## Servers

Run a `git-server` from docker:

```sh
docker run -d -p 2222:22 --name git-server -v ~/git-server/keys:/git-server/keys -v ~/git-server/repos:/git-server/repos jkarlos/git-server-docker
```

Run a `jenkins` from docker:

```sh
docker run --detach --name jenkins -p 8082:8080 -p 50000:50000 -v ./jenkins_home:/var/jenkins_home jenkins/jenkins:lts:wq
```

Create a docker network between git server and jenkins

## Authentication

Generate 2 key pair one for jenkins et on fake for user:

```sh
ssh-keygen -t rsa -b 4096 -C "jenkins"
ssh-keygen -t rsa -b 4096 -C "user"
```

### jenkins

Add private key to jenkins credentials store

### git-server

Go to the `git-server` and copy-paste the two public key to the `authorized_keys` file.

```sh
docker exec -ti git-sever sh
```


```sh
$ cd ~/.ssh && cat > authorized_keys
```

Paste the two public keys then `Ctrl+D` "to save"

## Jenkins Configuration

Add `git-server`as SSH server.
Url: `ssh://git@git-server:22`

Configure remote SSH server for sftp in section `Publish over SSH`
- Paste private key of `jenkins`
- Name: `git-sever`
- Hostname: `git-server` (docker container name)
- username: `git`
- remote directory: `/git-server/repos/`

## Jobs

### Job : create-course-for-user

The job has *2* input parameters:
- `USER` username of the user
- `COURSE` the hackjam name

The job has *1* step:

#### *Step 1:*
SSH Command to the `git-server`
Command:

```sh
mkdir -p /git-server/repos/$USER && cd /git-server/repos/$USER && git clone --bare --branch instructions  --single-branch --depth 1 https://SolidSnake--23@bitbucket.org/SolidSnake--23/$COURSE.git && cd $COURSE.git &&  git remote add local . && git remote add sources https://SolidSnake--23@bitbucket.org/SolidSnake--23/$COURSE.git
```

### Job: lock-user-lesson-repo

The job has *2* input parameters:
- `USER` username of the user
- `COURSE` the hackjam name

The job has *2* steps:

#### *Step 1:*
Download sources code from git from `https://SolidSnake--23@bitbucket.org/SolidSnake--23/git-hooks.git`

#### *Step 2:*
Send files over SSH `git-server`
Sources files : `pre-receive allowed_branches`
Remote directory: `$USER/$COURSE.git/hooks/`

### Job: lock-user-lesson-repo

The job has *2* input parameters:
- `USER` username of the user
- `COURSE` the hackjam name
- `LESSON` the branch name to unlock

The job has *2* steps:
#### *Step 1:*
SSH Command to the `git-server`
Command:

```sh
echo $LESSON >> /git-server/repos/$USER/$COURSE.git/hooks/allowed_branches
```

#### *Step 2:*
SSH Command to the `git-server`
Command:

```sh
cd  /git-server/repos/$USER/$COURSE.git/ && git fetch --all && git push local remotes/sources/$LESSON:refs/heads/$LESSON
```
